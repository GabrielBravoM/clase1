API REST
ES UN PATRON DE DISEÑO QUE EXPONE RECURSOS A TRAVES DE UNA URL PARA REALIZAR OPERACIONES SOBRE ESTE.

C Create
R Read
U Update
D Delete

RECURSO: Instrumento

VERBOS
GET: read
POST: create
PUT: update
DELETE: delete
PATCH: partial update

GET:
http://YYY/api/instrumentos
http://YYY/api/instrumentos/1

http://YYY/api/instrumentos/1/tiposOperacion/10

POST
http://YYY/api/instrumentos

PUT
http://YYY/api/instrumentos/ID

DELETE
http://YYY/api/instrumentos/ID

www.banco.cl/api/

OBTENER TODAS LAS PERSONAS REGISTRADAS
GET
www.banco.cl/api/personas

OBTENER LA PERSONA CON ID 20
GET
www.banco.cl/api/personas/20

CREAR PERSONA
POST
www.banco.cl/api/personas

MODIFICAR PERSONA ID 100
PUT
www.banco.cl/api/personas/100

OBTENER CUENTAS CORRIENTES DE LA PERSONA ID 25
GET
www.banco.cl/api/personas/25/cuentascorrientes

MODIFICAR LA CUENTA CORRIENTE 1 DE LA PERSONA CON ID 30
PUT
www.banco.cl/api/personas/30/cuentascorrientes/1

CREAR PROYECTO POR DEFECTO (crear archivo package.json)
npm init -y
NOTA: SE DEBE TENER INSTALADO NODE

INSTALAR DEPENDENCIAS (PAQUETES EXTERNOS)
npm install NOMBRE_DEPENDENCIA
npm i

BUSCADOR DE DEPENDENCIAS
npmjs.com

NODEMON: REINICIA EL SERVIDOR POR CADA CAMBIO
nodemon server/server

JSON: ESTRUCTURA REPRESENTATIVA DE DATOS
{
"nombre": "juan",
"apellido": "perez"
}

PETICION HTTP
REQUEST
RESPONSE

CADA UNA TIENE 2 PARTES:

1. HEADER: TOKEN, TIPO DE FORMATO, ETC
2. BODY: DATOS A PROCESAR EN EL SERVIDOR

NPM INSTALL NOMBRE_DEPENDENCIA
INSTALA LAS DEPENDENCIAS DE PACKAGE.JSON

CLONAR PROYECTO

1. CREAR CARPETA CLASES
2. EN LA TERMINAL UBICARSE EN ESA CARPETA
3. USAR COMANDO GIT CLONE https://gitlab.com/GabrielBravoM/clase1.git
4. EN LA CARPETA DEL PROYECTO EJECUTAR npm install
5. ARRANCAR EL PROYECTO CON npm start

PACKAGE.JSON
"dependencies": son las dependencias que usa el proyecto para su ejecucion
"devDevendencies": son las dependencias que se usan para desarrollo

@BABEL/preset-env ES UN COMPLIADOR DE JAVASRIPT

CI/CD
Continous integration / Continous Delivery
GIT LAB => PULL REQUEST OK
BRANCH1 => BRANCH1
BRANCH2 => BRANCH2

DEVELOP
MASTER

MONGODB = NOSQL => NO ES RELACIONAL
Almacena datos en formato json en forma de colecciones.

EJ FACTURA EN SQL
ENCABEZADO: num factura, fecha, cliente, bla bla
DETALLE: num factura, cod, nombre, valor

EJ FACTURA EN NO SQL
{
numFactura,
fecha,
cliente,
detalle: [
codigo,
nombre,
valor
]
}

MONGOOSE: libreria para conectar a mongodb
