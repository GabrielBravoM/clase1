const { exists } = require("./../models/client");
const Client = require("./../models/client");
const {
  getAllClients,
  saveClient,
  getByRut,
  deleteClient,
  getById,
} = require("./../servicesDB/clientsService");

const existClient = (rut) => {
  return true;
};

const calcularEdad = (anio_nacimiento) => {
  var anio_actual = new Date().getFullYear();
  return anio_actual - anio_nacimiento;
};

class ClientsController {
  static async getAll(req, res, next) {
    const clients = await getAllClients();
    res.status(200).send({
      clients,
    });
  }

  static async createClient(req, res, next) {
    const { rut, nombre, apellido, email, anio_nacimiento } = req.body;

    const existsClient = await getByRut(rut);
    //TRULLY OR FALSY
    //EJ variable = {algo} => TRUE
    //EJ variable = null => FALSE
    if (existsClient) {
      res.status(400).send({
        err: "El RUT ya existe",
      });
      return;
    }

    const edad = await calcularEdad(anio_nacimiento);

    const newClient = new Client({
      rut,
      nombre,
      apellido,
      email,
      edad,
    });

    const createdClient = await saveClient(newClient);

    res.status(201).send({
      cliente: createdClient,
    });
  }

  //TAREA PARA LA CASA
  //1.- RESCATAR EL ID DE LOS PARAMS (IGUAL QUE EN EL DELETE)
  //2.- RESCATAR LOS DATOS DEL CLIENTE DEL BODY (IGUAL QUE EN EL CREATE)
  //3.- VALIDAR SI EL ID EXISTE
  //4.- SI EXISTE EL ID MODIFICAR LOS DATOS Y ENVIAR A GUARDAR CON EL MISMO METODO DEL CREATE
  //5.- RETORNAR STATUS 200
  static updateClient(req, res, next) {
    res.status(200).send({
      actualizado: true,
    });
  }

  static async deleteClient(req, res, next) {
    const { idCliente } = req.params;

    const existsClient = await getById(idCliente);
    if (!existsClient) {
      res.status(400).send({
        err: "El Id no existe",
      });
      return;
    }

    const deletedClient = await deleteClient(idCliente);

    res.status(200).send({
      client: deletedClient,
    });
  }
}

module.exports = { ClientsController, calcularEdad };
