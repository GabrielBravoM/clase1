import { calcularEdad } from "./clientsController";

test("calcular edad a partir del año de nacimiento", () => {
  expect(calcularEdad(2000)).toBe(21);
});
