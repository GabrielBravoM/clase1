const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const clientSchema = new Schema({
  rut: String,
  nombre: String,
  apellido: String,
  email: String,
  edad: Number,
});

module.exports = mongoose.model("Client", clientSchema);
