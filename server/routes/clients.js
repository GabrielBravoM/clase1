const express = require("express");
const app = express();
const { ClientsController } = require("./../controller/clientsController");

app.get("/clients", ClientsController.getAll);
app.post("/clients", ClientsController.createClient);
app.put("/clients/:idCliente", ClientsController.updateClient);
app.delete("/clients/:idCliente", ClientsController.deleteClient);

// //CREAR UN RECURSO Y LOS DATOS VIENEN EN EL BODY
// app.post('/clients', function (req, res) {
//     const { rut, nombre, apellido, email } = req.body;

//     res
//     .status(201)
//     .send({
//         nombre,
//         apellido,
//         email
//     })
// });

// //ACTUALZIAR RECURSO
// app.put('/clients/:id', function (req, res) {
//     const id = req.params.id;

//     const { nombre, apellido, email } = req.body;

//     res
//     .status(200)
//     .send({
//         id,
//         nombre,
//         apellido,
//         email
//     })
// });

// app.delete('/clients/:id', function (req, res) {
//     const id = req.params.id;

//     res
//     .status(200)
//     .send({
//         id
//     })
// });

module.exports = app;

//PATRON SINGLETON
/*
const instancia = null;
if(instancia==null)
    return new Object();
else 
    return instancia;
*/
