const express = require('express')
const app = express()

app.use(require('./clients'));

module.exports = app;