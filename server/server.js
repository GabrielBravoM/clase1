require("dotenv").config();
//libreria para exponer rutas en un servidor web
const express = require("express");
const mongoose = require("mongoose");
//inicialziar app con la instancia de express
const app = express();

//para manejar los datos del body en formato json
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.use(require("./routes/index"));

mongoose
  .connect("mongodb://localhost:27017/clase-mongo-db", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoCreate: true,
  })
  .then(() => {
    console.log("conectado a MONGODB");
  })
  .catch((err) => {
    console.log(err);
  });

//DEFINICION DE LAS RUTAS

//GET PARA OBTENER RECURSOS
//CADA PETICION TIENE UN REQUEST Y UN RESPONSE
//REQUEST ES LO QUE SE ENVIA AL SERVIDOR
//RESPONSE ES LO QUE EL SERVIDOR RESPONDE

//CADA REQUEST Y RESPONSE TIENE UN HEADER Y UN BODY

//EL RESPONSE (RES) TIENE UN CODIGO DE STATUS QUE RESPRESENTA EL ESTADO DEL REQUEST (200, 201, 404, etc)
//EL RESPONSE TIENE LA FUNCION SEND QUE ES LO QUE LE RETORNA AL CLIENTE. ESTE ES UN OBJETO JSON

//process.env es variable global del proyecto de node
const port = process.env.PORT;

app.listen(port, () => {
  console.log("Servidor corriento en puerto " + port);
});
