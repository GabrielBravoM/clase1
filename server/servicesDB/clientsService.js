const Client = require("./../models/client");

const getAllClients = () => {
  return new Promise((resolve, reject) => {
    Client.find().exec((err, clients) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(clients);
    });
  });
};

const getById = (id) => {
  return new Promise((resolve, reject) => {
    Client.findById(id).exec((err, client) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(client);
    });
  });
};

const getByRut = (rut) => {
  return new Promise((resolve, reject) => {
    Client.findOne({ rut }).exec((err, client) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(client);
    });
  });
};

//ESTE MEDOTO HACE 2 COSAS: CREATE O UPDATE DEPENDIENDO SI EL OBJETO client TIENE _ID
const saveClient = (client) => {
  return new Promise((resolve, reject) => {
    client.save((err, clientSaved) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(clientSaved);
    });
  });
};

const deleteClient = (id) => {
  return new Promise((resolve, reject) => {
    Client.findByIdAndDelete({ _id: id }).exec((err, clientDeleted) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(clientDeleted);
    });
  });
};

module.exports = { getAllClients, saveClient, getByRut, deleteClient, getById };
